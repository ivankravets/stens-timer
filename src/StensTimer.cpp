/*
Copyright (C) 2017  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <StensTimer.h>

StensTimer* StensTimer::_instance = NULL;

StensTimer* StensTimer::getInstance(){
    if (_instance == NULL){ // prevents double initialization
        _instance = new StensTimer();
    }
    return _instance;
}

StensTimer::StensTimer(){}

/* delete timers before object will be destroyed */
StensTimer::~StensTimer(){
  deleteTimers();
  _instance = NULL;
}

/* creates new timer and returns its pointer */
Timer* StensTimer::createTimer(IStensTimerListener* listener, int action, long interval, int repetitions){
  int freeSlot = findFreeSlot();

  /* no space for more timers */
  if(freeSlot < 0){
    Serial.println(F("Error: Timer slots full!"));
    return NULL;
  }

  /* increment _lastId by one, making always unique */
  _lastId++;
  _timers[freeSlot] = new Timer(listener, _lastId, action, interval, repetitions);
  return _timers[freeSlot];
}

/* creates and returns timer that runs once */
Timer* StensTimer::setTimer(IStensTimerListener* listener, int action, long delay){
  return createTimer(listener, action, delay, 1);
}

/* creates and returns timer that runs forever */
Timer* StensTimer::setInterval(IStensTimerListener* listener, int action, long interval){
  return createTimer(listener, action, interval, 0);
}

/* returns first index of empty timer slot, if full, return -1 */
int StensTimer::findFreeSlot(){
  for(int index = 0; index < MAX_TIMERS; index++){
    if(_timers[index] == NULL){
      return index;
    }
  }
  return -1;
}

/* delete given timer by searching in timers and then deleting it */
void StensTimer::deleteTimer(Timer* timer){
  if(timer == NULL){
    return;
  }

  /* check if timer is in timers array, and delete it */
  for(int i = 0; i < MAX_TIMERS; i++){
    if(_timers[i]->equals(timer)){
      delete _timers[i];
      _timers[i] = NULL;
      return;
    }
  }
}

/* delete all timers by looping through array and call delete */
void StensTimer::deleteTimers(){
  for(int i = 0; i < MAX_TIMERS; i++){
    if(_timers[i] != NULL){
      delete _timers[i];
      _timers[i] = NULL;
    }
  }
}

void StensTimer::run(){

  long now = millis();

  /* execute callback for every timer slot that is not null */
  for(int i = 0; i < MAX_TIMERS; i++){

    /* skip iteration if no timer exists in this slot */
    if(_timers[i] == NULL){
      continue;
    }

    /* for readability create temporary variable */
    Timer* timer = _timers[i];

    /* skip timer instance if time is not yet due */
    if(now < timer->getLastCall() + timer->getDelay()){
      continue;
    }

    /* update last call time */
    timer->setLastCall(now);

    int repetitions = timer->getRepetitions();

    /* pass timer to callback function implemented by user */
    timer->getListener()->timerCallback(timer);

    /* check if timer is done repeating, if so, delete it */
    if(repetitions > 1){
      timer->setRepetitions(repetitions - 1);
    }
    else if(repetitions == 1){
      delete _timers[i];
      _timers[i] = NULL;
    }

  }

}
